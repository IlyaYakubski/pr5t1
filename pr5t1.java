import java.util.Arrays;

public class pr5t1 {
    public static char[] selection(char[] arrayOne) {
        int pos;
        int temp;
        for (int i = 0; i < arrayOne.length; i++) {
            pos = i;
            for (int j = i + 1; j < arrayOne.length; j++) {
                if (arrayOne[j] < arrayOne[pos]) {
                    pos = j;
                }
            }

            temp = arrayOne[pos];
            arrayOne[pos] = arrayOne[i];
            arrayOne[i] = (char) temp;
        }
        return arrayOne;
    }

    public static int[] bubble(int[] arrayTwo) {
        int temp;
        boolean sort = false;
        while (!sort) {
            sort = true;
            for (int t = 0; t < arrayTwo.length - 1; t++) {
                if (arrayTwo[t] > arrayTwo[t + 1]) {
                    sort = false;
                    temp = arrayTwo[t];
                    arrayTwo[t] = arrayTwo[t + 1];
                    arrayTwo[t + 1] = temp;
                }
            }

        }

        return arrayTwo;
    }

    public static int[] insertion(int[] arrayThree) {
        for (int p = 1; p < arrayThree.length; p++) {
            int curr = arrayThree[p];
            int pr = p - 1;
            while (pr >= 0 && arrayThree[pr] > curr) {
                arrayThree[pr + 1] = arrayThree[pr];
                arrayThree[pr] = curr;
                pr--;
            }
        }
        return arrayThree;
    }

    public static void main(String[] args) {

        char[] arrayOne = new char[]{'k', 'p', 'q', 'd', 'p', 'r', 'k', 'u', 'a', 'c', 'b', 'w'};
        arrayOne = selection(arrayOne);
        System.out.println(Arrays.toString(arrayOne));
        int[] arrayTwo = new int[]{3, 6, 1, 5, 9, 1984, 47, 53, 16};
        arrayTwo = bubble(arrayTwo);
        System.out.println(Arrays.toString(arrayTwo));
        int[] arrayThree = new int[]{1900, 1999, 1917, 1939, 1991, 1984, 1961, 1986, 1914};
        arrayThree = insertion(arrayThree);
        System.out.println(Arrays.toString(arrayThree));
    }
}
